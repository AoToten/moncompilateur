//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

unsigned long tagNumber = 0;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {UNSIGNED_INT, BOOL, DOUBLE, CHAR};

map<string, enum TYPES> declaredVariables;

TOKEN current; //Current token

FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

//Prototypes des fonctions
enum TYPES Identifier(void);
bool isDeclared(const char *id);
void Error(string s);
enum OPREL RelationalOperator(void);
enum OPADD AdditiveOperator(void);
enum OPMUL MultiplicativeOperator(void);
enum TYPES Number(void);
enum TYPES Factor(void);
enum TYPES SimpleExpression(void);
enum TYPES Expression(void);
enum TYPES Term(void);
void AssignementStatement(void);
void BlockStatement(void);
void ForStatement(void);
void WhileStatement(void);
void IfStatement(void);
void Statement(void);
void StatementPart(void);
void VarDeclaration(void);
void VarDeclarationPart(void);
void Program(void);

enum TYPES Identifier(void){
	if(!isDeclared(lexer->YYText())){
		cerr<<"Variable "<< lexer->YYText()<<" non déclarée"<<endl;
		exit(-1);
	}
	enum TYPES type = declaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}


bool isDeclared(const char *id){
	return declaredVariables.find(id) != declaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// ArithmeticExpression := Term {AdditiveOperator Term}
// Term := Digit | "(" ArithmeticExpression ")"
// AdditiveOperator := "+" | "-"
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"

		

// <OpérateurRelationnel> ::= '=' | '<>' | '<' | '<=' | '>=' | '>' 
enum OPREL RelationalOperator(void){  
	//cout<<"RelationalOperator "<<current<<endl;
	OPREL op;
	if(strcmp(lexer->YYText(), "==") == 0){
		op = EQU;
	}else if(strcmp(lexer->YYText(), "<=") == 0){
		op = INFE;
	}else if(strcmp(lexer->YYText(), ">=") == 0){
		op = SUPE;
	}else if(strcmp(lexer->YYText(), "<") == 0){
		op = INF;
	}else if(strcmp(lexer->YYText(), ">") == 0){
		op = SUP;
	}else if(strcmp(lexer->YYText(), "<>") == 0){
		op = DIFF;
	}else{
		op = WTFR;
	}
	current=(TOKEN) lexer->yylex();
	return op;
}

// AdditiveOperator := "+" | "-" | "||"
enum OPADD AdditiveOperator(void){
	//cout<<"AdditiveOperator "<<current<<endl;
	OPADD op;
	if(strcmp(lexer->YYText(), "+") == 0){
		op = ADD;
	}else if(strcmp(lexer->YYText(), "-") == 0){
		op = SUB;
	}else if(strcmp(lexer->YYText(), "||") == 0){
		op = OR;
	}else{
		op = WTFA;
	}
	current=(TOKEN) lexer->yylex();
	return op;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
enum OPMUL MultiplicativeOperator(void){
	//cout<<"MultiplicativeOperator "<<current<<endl;
	OPMUL op;
	if(strcmp(lexer->YYText(), "*") == 0){
		op = MUL;
	}else if(strcmp(lexer->YYText(), "/") == 0){
		op = DIV;
	}else if(strcmp(lexer->YYText(), "%") == 0){
		op = MOD;
	}else if(strcmp(lexer->YYText(), "&&") == 0){
		op = AND;
	}else{
		op = WTFM;
	}
	current=(TOKEN) lexer->yylex();
	return op;
}

enum TYPES Number(void){
	string number = lexer->YYText();
	if(number.find(".") != string::npos){
		//Ca veut dire que c'est un flottant

		//Le morceau de code en commentaire ne marchait pas donc j'ai décidé de faire comme sur votre framagit
		//double number = atof(lexer->YYText()); //On transforme le texte en double
		//long long unsigned int *i; // i est un pointeur sur un espace de 8 octets sensé contenir un entier non signé en binaire naturel
		//i = (long long unsigned int *) &number; // Maintenant, *i est un entier non signé sur 64 bits qui contient l'interprétation entière du codage IEEE754 du flottant 'number'
		//cerr<<*i<<endl;
		//cout << "\tpush $"<<*i<<"\t# empile le flottant "<<number<<endl;

		double number = atof(lexer->YYText());
		unsigned int *i = (unsigned int *) &number; // i points to the const double 
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)\t# Conversion of "<<number<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<number<<" (32 bit low part)"<<endl;
		current = (TOKEN) lexer->yylex();
		return DOUBLE;
	} else{
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return UNSIGNED_INT;
	}
}

enum TYPES Char(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl; //Je prend l'octet correspondant aux caractère et je le met dans la partie basse du registre ax 
	cout<<"\tpush %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl; //J'envoie la version 64bits du caractère correspondant
	current=(TOKEN) lexer->yylex();
	return CHAR;
}


// Factor := Number | ID | "(" Expression ")"| "!" Factor
enum TYPES Factor(void){
	enum TYPES type;
	if(current == NUMBER){
		type = Number();
	}else if(current == ID){
		type = Identifier();
	}else if(current == CHAR_){
		type = Char();
	}else if(current == LPARENT) { //J'ai inversé LPARENT et RPARENT (pareil pour les crochets) parce que ça me semblait plus logique que '(' soit LPARENT
		current = (TOKEN) lexer->yylex();
		type = Expression();
		if(current != RPARENT){
			Error(" ')' était attentedu ");
		}else{
			current = (TOKEN) lexer->yylex();
		}

	}else if(current == NOT){
		current = (TOKEN) lexer->yylex();
		type = Factor();
	}
	return type;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(void){

	enum TYPES typeTerm1, typeTerm2;
	OPADD op;
	typeTerm1 = Term();
	while(current == ADDOP){
		op = AdditiveOperator();
		typeTerm2 = Term();
		if(typeTerm2 != typeTerm1){
			cerr<<typeTerm1<<endl;
			cerr<<typeTerm2<<endl;
			Error("Il faut que les termes soient du même type");
		}
		cout << "\tpop %rbx"<<endl;	
		cout << "\tpop %rax"<<endl;	

		switch(op){
			case OR:
				if(typeTerm2!=BOOL)
					Error("opérande non booléenne pour l'opérateur OR");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\torq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax"<<endl;			// store result
				break;			
			case ADD:
				if(typeTerm2!=UNSIGNED_INT&&typeTerm2!=DOUBLE)
					Error("opérande non numérique pour l'addition");
				if(typeTerm2==UNSIGNED_INT){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;			
			case SUB:	
				if(typeTerm2!=UNSIGNED_INT&&typeTerm2!=DOUBLE)
					Error("opérande non numérique pour la soustraction");
				if(typeTerm2==UNSIGNED_INT){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tsubq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfsubp	%st(0),%st(1)\t# %st(0) <- op1 - op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;	
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;
	}
	return typeTerm1; //On peut aussi return le typeTerm2 parce que ce sont les même sinon il y a une erreur

}

// <OpérateurRelationnel> ::= '=' | '<>' | '<' | '<=' | '>=' | '>' 
// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void){

	enum TYPES typeSExpr1, typeSExpr2;
	enum OPREL op;
	typeSExpr1 = SimpleExpression();
	unsigned long tag;
	while(current == RELOP){
		op = RelationalOperator();
		typeSExpr2 = SimpleExpression();
		tag = ++tagNumber;
		if(typeSExpr1 != typeSExpr2){
			Error("Les deux expressions doivent avoir le même type pour pouvoir effectuer une opération");
		}
		if(typeSExpr1 != DOUBLE){
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}else{
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t# pop nothing"<<endl;
		}

		switch(op){
			case EQU:
				cout << "\tje Vrai"<<tag<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<tag<<"\t# If different"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<tag<<"\t# If below"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<tag<<"\t# If below or equal"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<tag<<"\t# If above or equal"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<tag<<"\t# If above"<<endl;
				break;
			default:
				return typeSExpr1;
			
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<tag<<endl;
		cout << "Vrai"<<tag<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<tag<<":"<<endl;
		return BOOL;
	}
	return typeSExpr1;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void){

	enum TYPES typeFactor1, typeFactor2;
	OPMUL op;
	typeFactor1 = Factor();
	while(current == MULOP){
		op = MultiplicativeOperator();
		typeFactor2 = Factor();
		if(typeFactor1 !=  typeFactor2){
			Error("Vous essayez d'effectuer une opération sur 2 variables de types différents...");
		}
		switch(op){
			case AND:
				if(typeFactor2 != BOOL){
					Error("Pour effectuer l'operation AND il faut que les deux variables soient des booleens");
				}
				cout << "\tpop %rbx"<<endl;	
				cout << "\tpop %rax"<<endl;	
				cout << "\tmulq	%rbx"<<endl;	// a AND b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(typeFactor2 != UNSIGNED_INT && typeFactor2 != DOUBLE){
					Error("Pour effectuer l'operation MUL il faut que les deux variables soient des nombres");
				}
				if(typeFactor2 == UNSIGNED_INT){
					cout << "\tpop %rbx"<<endl;	
					cout << "\tpop %rax"<<endl;	
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case DIV:
				if(typeFactor2 != UNSIGNED_INT && typeFactor2 != DOUBLE){
					Error("Pour effectuer l'operation DIV il faut que les deux variables soient des nombres");
				}
				if(typeFactor2 == UNSIGNED_INT){
					cout << "\tpop %rbx"<<endl;	
					cout << "\tpop %rax"<<endl;	
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case MOD:
				if(typeFactor2 != UNSIGNED_INT){
					Error("Pour effectuer l'operation MOD il faut que ce soit un entier");
				}
				cout << "\tpop %rbx"<<endl;	
				cout << "\tpop %rax"<<endl;	
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return typeFactor1; //On peut aussi return le typeFactor2 parce que ce sont les même sinon il y a une erreur
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	//cout<<"AssignementStatement "<<current<<endl;
	enum TYPES typeID, typeExpression;
	string variable;
	if(current != ID){
		Error("Identificateur attendu");
	}
	if(!isDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable = lexer->YYText();
	typeID = declaredVariables[variable];
	current = (TOKEN) lexer->yylex();
	if(current != ASSIGN){
		Error("Caractère d'assignement ':=' attendu ici");
	}
	current = (TOKEN) lexer->yylex();
	typeExpression = Expression();
	if(typeExpression != typeID){
		cerr<<"Type variable : "<<typeID<<endl;
		cerr<<"Type expression : "<<typeExpression<<endl;
		Error("Types incompatibles");
	}
	if(typeID == CHAR){
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl;
	}else{
		cout << "\tpop "<<variable<<endl;
	}
}

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void){
	current = (TOKEN) lexer->yylex();
	Statement();
	while(current == SEMICOLON){
		current = (TOKEN) lexer->yylex();
		Statement();
	}
	if(current != KEYWORD || strcmp(lexer->YYText(), "END") != 0){
		Error("END attendu ici");
	}
	current = (TOKEN) lexer->yylex();
}

//ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement
void ForStatement(void){
	current = (TOKEN) lexer->yylex();
	string variable = lexer->YYText();
	AssignementStatement();
	if(current != KEYWORD || strcmp(lexer->YYText(), "TO") != 0){
		Error("TO attendu ici");
	}
	current = (TOKEN) lexer->yylex();
	enum TYPES type = Expression();
	unsigned long tag = tagNumber++;
	if(Expression() != UNSIGNED_INT){
		Error("UNSIGNED_INT attendu ici car nous voulons incrémenté jusqu'à cette valeur");
	}
	cout<<"For"<<tag<<":"<<endl;
	cout<<"\tpush $10"<<endl; //Je répète car sinon r8 se reinitialise à 0
	cout<<"\tpop %r8\t# Get the result of expression"<<endl;
	cout<<"\tpush "<<variable<<endl; 
	cout<<"\tpop %rbx\t# Put the variable value into rbx"<<endl;
	cout<<"\tcmp %rbx, %r8\t# On compare notre compteur et l'expression"<<endl;
	cout<<"\tje EndFor"<<tag<<"\t# if FALSE, jump out of the loop"<<tag<<endl;
	if(current != KEYWORD || strcmp(lexer->YYText(), "DO") != 0)
		Error("mot-clé DO attendu");
	current = (TOKEN) lexer->yylex();
	Statement();
	cout<<"\tinc %rbx\t# Increment the value of the variable"<<endl;
	cout<<"\tpush %rbx"<<endl;
	cout<<"\tpop "<<variable<<"\t# Put back the value into the variable"<<endl;
	cout<<"\tjmp For"<<tag<<endl;
	cout<<"EndFor"<<tag<<":"<<endl;
}

//WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(void){
	unsigned long tag = tagNumber++;
	current = (TOKEN) lexer->yylex();
	cout<<"While"<<tag<<":"<<endl;
	if(Expression() != BOOL){
		Error("Booleens attendu ici");
	}
	cout<<"\tpop %rax\t# Get the result of expression"<<endl;
	cout<<"\tcmpq $0, %rax"<<endl;
	cout<<"\tje EndWhile"<<tag<<"\t# if FALSE, jump out of the loop"<<tag<<endl;
	if(current != KEYWORD || strcmp(lexer->YYText(), "DO") != 0)
		Error("mot-clé DO attendu");
	current = (TOKEN) lexer->yylex();
	Statement();
	cout<<"\tjmp While"<<tag<<endl;
	cout<<"EndWhile"<<tag<<":"<<endl;
}

//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	current = (TOKEN) lexer->yylex();
	unsigned long tag = tagNumber++;
	if(Expression() != BOOL){
		Error("Booleens attendu ici");
	}
	cout<<"\tpop %rax\t# Get the result of expression"<<endl;
	cout<<"\tcmpq $0, %rax"<<endl;
	cout<<"\tje Else"<<tag<<"\t# if FALSE, jump to Else"<<tag<<endl;
	if(current != KEYWORD || strcmp(lexer->YYText(),"THEN") != 0)
		Error("mot-clé 'THEN' attendu");
	current = (TOKEN) lexer->yylex();
	Statement();
	cout<<"\tjmp Next"<<tag<<"\t# Do not execute the else statement"<<endl;
	cout<<"Else"<<tag<<":"<<endl; // Might be the same effective adress than Next:
	if(current == KEYWORD && strcmp(lexer->YYText(),"ELSE") == 0){
		current = (TOKEN) lexer->yylex();
		Statement();
	}
	cout<<"Next"<<tag<<":"<<endl;
}

// DisplayStatement := "DISPLAY" Expression
void DisplayStatement(void){
	enum TYPES typeExp;
	unsigned long tag=++tagNumber;
	current=(TOKEN) lexer->yylex();
	typeExp = Expression();
	switch(typeExp){
	case UNSIGNED_INT:
		cout << "\tpop %rsi\t# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
		break;
	case BOOL:
		cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
		cout << "\tcmpq $0, %rdx"<<endl;
		cout << "\tje False"<<tag<<endl;
		cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
		cout << "\tjmp Next"<<tag<<endl;
		cout << "False"<<tag<<":"<<endl;
		cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
		cout << "Next"<<tag<<":"<<endl;
		cout << "\tcall	puts@PLT"<<endl;
			break;
	case DOUBLE:
		cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
		cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
		cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
		cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
		cout << "\tmovq	$1, %rax"<<endl;
		cout << "\tcall	printf"<<endl;
		cout << "nop"<<endl;
		cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
		break;
	case CHAR :
		cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
		cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
		break;
	default:
			Error("Impossible d'afficher la donnée");
		}

}


//Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement
void Statement(void){
	if(current == ID){
		AssignementStatement();
	}else{
		if(current == KEYWORD){
			if(strcmp(lexer->YYText(), "IF") == 0){
				IfStatement();
			}else if(strcmp(lexer->YYText(), "FOR") == 0){
				ForStatement();
			}else if(strcmp(lexer->YYText(), "WHILE") == 0){
				WhileStatement();
			}else if(strcmp(lexer->YYText(), "BEGIN") == 0){
				BlockStatement();
			}else if(strcmp(lexer->YYText(), "DISPLAY")== 0){
				DisplayStatement();
			}
		}else {
			Error("Instruction attendu ici");
		}
	}
}


// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	//cout<<"StatementPart "<<current<<endl;
	cout << "\t.align 8"<<endl;	// Alignement on addresses that are a multiple of 8 (64 bits = 8 bytes)
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current == SEMICOLON){
		current = (TOKEN) lexer->yylex();
		Statement();
	}
	if(current != DOT){
		Error("Un '.' est attendu ici");
	}
	current = (TOKEN) lexer->yylex();
}

enum TYPES GetType(){
	if(current != KEYWORD){
		Error("Keyword attendu ici : type");
	}
	if(strcmp(lexer->YYText(), "UNSIGNED_INT") == 0){
		return UNSIGNED_INT;
	}else if(strcmp(lexer->YYText(), "BOOL") == 0){
		return BOOL;
	}else if(strcmp(lexer->YYText(), "DOUBLE") == 0){
		return DOUBLE;
	}else if(strcmp(lexer->YYText(), "CHAR") == 0){
		return CHAR;
	}else{
		Error("Type non répertorié");
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	current = (TOKEN) lexer->yylex();
	VarDeclaration();
	while(current == SEMICOLON){
		current = (TOKEN) lexer->yylex();
		VarDeclaration();
	}
	current = (TOKEN) lexer->yylex();
	if(current != DOT){
		Error("'.' attendu ici");
	}
	current = (TOKEN) lexer->yylex();
}


// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	set<string> ids;
	if(current != ID){
		Error("Identificateur attendu ici");
	}
	ids.insert(lexer->YYText());
	current = (TOKEN) lexer->yylex();
	while(current == COMMA){
		current = (TOKEN) lexer->yylex();
		if(current != ID){
			Error("Identificateur attendu ici");
		}
		ids.insert(lexer->YYText());
		current = (TOKEN) lexer->yylex();
	}
	if(current != COLON){
		Error("':' attendu ici");
	}
	current = (TOKEN) lexer->yylex();
	enum TYPES typeVar = GetType();
	for (set<string>::iterator i=ids.begin(); i!=ids.end(); ++i){
		if(typeVar == UNSIGNED_INT || typeVar == BOOL){
			cout<< *i << ":\t.quad 0"<<endl;
		}else if(typeVar == DOUBLE){
			cout<< *i << ":\t.double 0.0"<<endl;
		}else if(typeVar == CHAR){
			cout<< *i << ":\t.byte 0"<<endl;
		}
		declaredVariables[*i] = typeVar;
	}
}


// Program := VarDeclarationPart StatementPart
void Program(void){
	if(current != KEYWORD){
		Error("Keyword attendu ici");
	}
	if(strcmp(lexer->YYText(), "VAR") == 0){
		VarDeclarationPart();
	}
	StatementPart();
}


int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\\n\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\\n\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\\n\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\\n\"\t# used by printf to display the boolean value FALSE"<<endl; 
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
	


		
			





